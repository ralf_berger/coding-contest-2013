#!/usr/bin/env ruby
# coding: utf-8
require 'minitest/spec'
require 'minitest/autorun'

require File.join(File.dirname(__FILE__), '..', 'lib', 'dna')

describe Dna do
  it 'must sanitize strings' do
    crap  = "agcctACCGT\nxyz!\n"
    clean = "agcctaccgt"
    Dna.new(crap).sequence.must_equal clean
  end

  it 'must detect Tyberius syndrome risk' do
    Dna.new('aggtggcgga').tyberius?.must_equal false
    Dna.new('agggtgggcggga').tyberius?.must_equal true
  end

  it 'must detect brown eyes' do
    Dna.new('cagcgt').brown_eyed?.must_equal false
    Dna.new('cagcgc').brown_eyed?.must_equal true
  end

  it 'must find the first CTAG sequence' do
    Dna.new('tagttctagaa').locate('ctag').must_equal 6
  end

  it 'must count nucleobases' do
    Dna.new('agggtgggcggga').count(:a).must_equal 2
    Dna.new('agggtgggcggga').count(:g).must_equal 9
  end

  it 'must classify nucleobases' do
    Dna.new('cagcgt').class_sequence.must_equal 'yrryry'
  end

  it 'must count classified nucleobases' do
    Dna.new('cagcgtttttttttt').class_count(:r).must_equal 3
  end

  it 'must detect Frømingen\'s dischrypsia risk' do
    Dna.new('ctgagatt').dischrypsia?.must_equal false
    Dna.new('gagattct').dischrypsia?.must_equal true
  end

  it 'must have a complement' do
    Dna.new('gattaca').complement.must_equal 'tgtaatc'
  end
end
