#!/usr/bin/env ruby
# coding: utf-8

$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')
require 'dna'

def examine(dna)
  puts "Examining the following DNA sequence of #{dna.length} nucleobases:\n\n"
  puts dna.sequence, ''
  puts "Nucleobase A:     #{dna.count :a}"
  puts "Nucleobase C:     #{dna.count :c}"
  puts "Nucleobase G:     #{dna.count :g}"
  puts "Nucleobase T:     #{dna.count :t}"
  puts "Tyberius risk:    #{dna.tyberius?}"
  puts "Brown eyed:       #{dna.brown_eyed?}"
  puts "First CTAG seq:   #{dna.locate('ctag')}"
  puts "Purines (R):      #{dna.class_count :r}"
  puts "Pyrimidines (Y):  #{dna.class_count :y}"
  puts "More R then Y:    #{dna.class_count(:r) > dna.class_count(:y)}"
  puts "Dischrypsia risk: #{dna.dischrypsia?}"
  puts "\nComplementary sequence:\n\n"
  puts dna.complement
end

$stderr.puts 'Please provide a file name.' if ARGV.empty?
ARGV.each{ |f| examine Dna.new(File.open(f).read) }
