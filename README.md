# DNA Sequence

This is an entry for the [Coding Contest 2013](http://www.coding-contest.de/index.php?id=537). (Yay, won the second price! =)

## Functionality

The executable examines a given text file containing a DNA sequence according to the [Coding Contest 2013 tasks](http://www.coding-contest.de/aufgaben-5-coco/).

## Requirements

* Ruby 1.9.3

## Usage

Examine a file by passing the file name to the executable:

```
./bin/dna.rb data/example-sequence.txt
```

### Example run, using the provided file

```bash
$ ./bin/dna.rb data/example-sequence.txt
Examining the following DNA sequence of 1000 nucleobases:

ggaatttagggagttcccacattgcccagacgactcgtatagaattggtagttggccatgcgtccatatcacaaagacacagtccctggccgaccacactgtaaccacgaatatgccctatcgtacgggttgggatgcacttttgagttatacgcgctcgaatctatgcccagtacacatggtgccgacacctaactaggcagtgaggggcactcagacctgacatgagcggaagaaagaacccgcgggggccccacgacgtagcggcgacggctcaaccaatgccccgcccctttcataaggccaagcggactgggctttcgcccgagtctaaacccactgtatttaccattcatagtcaacagagggactttcaaaattcctaaactggttactgactaagaggaatcctcgcgctaatgaagacaacctccatagaggtcaaatggcgcgcagttgacttcagtattgaccttcttcagggtcccccatctttgatacttcacttatggacccggccaccgtgagttgaatcccggcgtccctcgcgtccccaacacagacaatatttttacgtgtccaagggcggaaagtgacgaggtgagaactggcgccgcgagaccggcccgatttctaataggcgggatagagatctgcccgacgcatttcacttgtagtcactcacggtatgactgtgcatgcactgaccgtcgctggcgtgtctttaatttaagctaggcttgacgtggagtgcagaatgaccatgttcaaggtgcttcggggctatatacttgggataaacgcgatcctgcggagtagcgtcgagaacaccgactgccgaatgtacaatccgcgtgacaatgccgaggctcgagatatcacttgaactgcgggcgaatcgattcgagagcccgatcgttaacaagtcgtcggctgtagccaataatatcttggttttagatcttgagtgtgggggcgtttacttaaccatccgaacgcg

Nucleobase A:     248
Nucleobase C:     265
Nucleobase G:     259
Nucleobase T:     228
Tyberius risk:    true
Brown eyed:       false
First CTAG seq:   196
Purines (R):      507
Pyrimidines (Y):  493
More R then Y:    true
Dischrypsia risk: true

Complementary sequence:

cgcgttcggatggttaagtaaacgcccccacactcaagatctaaaaccaagatattattggctacagccgacgacttgttaacgatcgggctctcgaatcgattcgcccgcagttcaagtgatatctcgagcctcggcattgtcacgcggattgtacattcggcagtcggtgttctcgacgctactccgcaggatcgcgtttatcccaagtatatagccccgaagcaccttgaacatggtcattctgcactccacgtcaagcctagcttaaattaaagacacgccagcgacggtcagtgcatgcacagtcataccgtgagtgactacaagtgaaatgcgtcgggcagatctctatcccgcctattagaaatcgggccggtctcgcggcgccagttctcacctcgtcactttccgcccttggacacgtaaaaatattgtctgtgttggggacgcgagggacgccgggattcaactcacggtggccgggtccataagtgaagtatcaaagatgggggaccctgaagaaggtcaatactgaagtcaactgcgcgccatttgacctctatggaggttgtcttcattagcgcgaggattcctcttagtcagtaaccagtttaggaattttgaaagtccctctgttgactatgaatggtaaatacagtgggtttagactcgggcgaaagcccagtccgcttggccttatgaaaggggcggggcattggttgagccgtcgccgctacgtcgtggggcccccgcgggttctttcttccgctcatgtcaggtctgagtgcccctcactgcctagttaggtgtcggcaccatgtgtactgggcatagattcgagcgcgtataactcaaaagtgcatcccaacccgtacgatagggcatattcgtggttacagtgtggtcggccagggactgtgtctttgtgatatggacgcatggccaactaccaattctatacgagtcgtctgggcaatgtgggaactccctaaattcc
```

## Running the test suite

The project uses MiniTest to ensure the given requirements are met.

### Example run

```bash
$ rake
Run options: --seed 12248

# Running tests:

.........

Finished tests in 0.001831s, 4915.3468 tests/s, 7099.9454 assertions/s.

9 tests, 13 assertions, 0 failures, 0 errors, 0 skips
```