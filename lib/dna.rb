class Dna
  def initialize(sequence)
    @sequence = sequence.gsub(/\s|[^acgt]/mi, '').downcase
  end

  def sequence
    @sequence
  end

  alias_method :to_s, :sequence

  def class_sequence
    @class_sequence ||= @sequence.gsub /./, {
      'a' => 'r', 'g' => 'r',
      'c' => 'y', 't' => 'y'
    }
  end

  def matches(expression)
    sequence.scan expression
  end

  def locate(expression, first_index = 1)
    sequence.index(expression) + first_index
  end

  def count(nucleobase)
    matches(nucleobase.to_s).count
  end

  def class_count(nucleobase)
    class_sequence.scan(nucleobase.to_s).count
  end

  def complement
    @complement ||= @sequence.reverse.gsub /./, {
      'a' => 't', 't' => 'a',
      'c' => 'g', 'g' => 'c',
    }
  end

  # Delegate calls to length() etc., so that we can treat it like a String
  def method_missing(method, *args, &block)
    sequence.send method, *args, &block
  end

  # This module contains certain patterns to detect within the DNA.
  module Characteristics
    def tyberius?
      matches('ggg').count >= 3
    end

    def brown_eyed?
      matches(/cag[cg][^t]{2}/).count >= 1
    end

    def dischrypsia?
      !!class_sequence.match('rrrryyyy')
    end
  end

  include Characteristics
end